from django.db import models

# Create your models here.
from django.db.models import CharField


class Product(models.Model):
    name = CharField(null=False, blank=False, max_length=10)
    price = models.FloatField(null=False)
