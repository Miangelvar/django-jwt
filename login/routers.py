from rest_framework.routers import DefaultRouter

from login.views import ProductViewSet

router = DefaultRouter()

router.register(r'product', ProductViewSet)